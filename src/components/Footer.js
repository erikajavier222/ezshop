import React from 'react';

const Footer = () => {
    return (  
      <div className="footer text-center">
     
        <p>Terms of Use               Privacy Policy</p>
        <p><em>@2022 Erika Corporation</em></p>
      </div>
  
  );
}
 
export default Footer;
