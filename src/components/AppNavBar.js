
import { useContext } from 'react';
import {Navbar, Nav, Container, Dropdown} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';

function AppNavBar(){
	const { user } = useContext(UserContext);
	return(
		<Navbar bg="dark" expand="lg" className=''>
			<Container>

				<Link to='/'><Navbar.Brand className='text-light ezshop'> EZ-SHOP </Navbar.Brand></Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse>
					<Nav className="ml-auto">
					
						{
							user.id && user.isAdmin ?
							<>								
								<Dropdown>
								  <Dropdown.Toggle variant="success" id="dropdown-basic">
								   Admin Panel
								  </Dropdown.Toggle>
									  <Dropdown.Menu>
									    <Dropdown.Item><Link to='/products/all' className="nav-link text-dark"> All Products</Link></Dropdown.Item>
									    <Dropdown.Item > <Link to='/products/create' className="nav-link text-dark">
									Create Product
								</Link></Dropdown.Item>
									  </Dropdown.Menu>
								</Dropdown>
								{/*<Link to='/products/create' className="nav-link text-light">
									Create Product
								</Link>
								<Link to='/products/all' className="nav-link text-light"> All Products</Link>*/}

							</>
							:
							<>
								<Link to='/' className="nav-link text-light">
								Home
								</Link>
							</>
						}
						
						<Link to='/products' className="nav-link text-light">
							Products
						</Link>
						{
							user.id !== null ?
								<Link to='/logout' className="nav-link text-light">
									Logout
								</Link>
							:
								<>
									<Link to='/register' className="nav-link text-light">
										Register
									</Link>
									<Link to='/login' className="nav-link text-light">
										Login
									</Link>
								</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
		);
};



export default AppNavBar;
