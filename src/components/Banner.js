//this component will be used as the hero section of our page
//responsive -> grid system
import {Row, Col} from 'react-bootstrap';
//we will use  default bootstrap utility classes to format the component

//Create a function that will describe the structure of the hero section

//class -> reserved keyword (HTML)
//React/JSX elements -> className

export default function Banner ({bannerData}){
	return(
		<Row className="pt-5 pb-5 px-1 text-center">
			<Col>
				<h1 className="mb-3 banner"> {bannerData.title} </h1>
				<p className="my-3 banner">{bannerData.content}</p>
			</Col>
		</Row>
		);
}

