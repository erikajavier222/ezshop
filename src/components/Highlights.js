//Grid System and Card
import {Container} from 'react-bootstrap'

//format the card with the help of utility classes of the bootstrap
export default function Highlights (){
	return(
		<Container className="highLights p-5">
			<h1>Welcome to EZ-SHOP!</h1>
			<h2>This shop was created for everyone who wants to shop easier and more convenient.</h2>
			<h3>Shop now and get more discounts especially for new users!</h3>
		</Container>
		);
}