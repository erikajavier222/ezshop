import {useState, useEffect} from 'react'
import Banner from './../components/Banner';

import {Row, Col, Card, Button, Container} from 'react-bootstrap';

import {/*Link, */useParams} from 'react-router-dom';

import Swal from 'sweetalert2';
import Footer from './../components/Footer';

export default function AdminProductView() {

	let token = localStorage.getItem('access');
	console.log(useParams())

	const {id} = useParams();
	console.log(id)
	
	const [productInfo, setProductInfo] = useState({
			name: null,
			description: null,
			price: null
		});

	const data = {
		title:`${productInfo.name}`,
		content:''
	}


	useEffect(() => {

		fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData => {
			console.log(convertedData)

			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		})
	},[id])

	const archiveProduct = async (isActive) => {

		await fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}/archive/`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
							isActive: false
						})
		})
		.then(result => result.json())
		.then(result => {

			if(result === true){
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Please Contact IT department",
				})
				
			} else {

				Swal.fire({
					title: "Success",
					icon: "success",
					text: `Product Archived Successfully!`,
				})
			}
		})
	};

		const reactivateProduct = async (isActive) => {

			await fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}/reactivation/`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
							isActive: true
						})
			})
			.then(result => result.json())
			.then(result => {
	
				if(result === true){
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Please Contact IT department"
	
					})
					
				} else {
					Swal.fire({
						title: "Success",
						icon: "success",
						text: `Successfully Reactivated the Product!`
					})
	
				}
			})
		};

		const deleteProduct = async () => {

			await fetch(`https://thawing-sea-49056.herokuapp.com/products/${id}`, {
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
	
				if(result === true){
					Swal.fire({
						title: "Successfuly Deleted!",
						icon: "success",
						text: `Product Deleted Successfully!`,
					})
					
				} else {
	
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please Contact IT department!",
					})
				}
			})
		};


	return(
		<>
			<Banner bannerData={data} />
			<div className="">
				
				<Row className="adminView">
					<Col md={7} sm={12} className=" mb-5">
						<Card className="cardImg container-fluid">
							{<Card.Img variant="top" className="img" src={`/${productInfo.name}.jpg`} />}
						</Card>
					</Col>
					<Col md={5} sm={12} className="">
						<Row>
							<Col>
								<Container>
									<Card className="text-center details alert alert-secondary">
										<Card.Body>
											{/*Product Name*/}
											<Card.Title>
												<h3>{productInfo.name}</h3>
											</Card.Title>
											
											{/*Product Desscription*/}
											<Card.Subtitle>
												<h6 className="my-4">Description: </h6>
											</Card.Subtitle>
											<Card.Text>
												{productInfo.description}
											</Card.Text>
											{/*Product Price*/}
											<Card.Subtitle className="my-4">
												<h6>Price:  </h6>
											</Card.Subtitle>
											<Card.Text>
												₱ {productInfo.price}
											</Card.Text>
										</Card.Body>
									
										
										
										{/*<Link className="btn btn-success btn-block mb-5" to="/login">
											Login to Order
										</Link>*/}
									</Card>
									
											<div className="my-5">
												
												<Button type="Submit" className="btn-warning btn-block" onClick={()=> archiveProduct()}>Archive</Button>	
											
												<Button type="Submit" className="btn-primary btn-block" onClick={()=> reactivateProduct()}>Reactivate</Button>	
											
												<Button type="Submit"  className="btn-danger btn-block" onClick={()=> deleteProduct()}>Delete</Button>	
											
											</div>

								</Container>
							</Col>
						</Row>
					</Col>
				</Row>		
			</div>
			<Footer />
			
		</>

			);
}