

import {useState, useEffect, useContext} from 'react'
import Hero from './../components/Banner';
import {Container, Form, Button, Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Footer from './../components/Footer';

const data = {
	title: 'Welcome to the Register Page',

};
export default function Register () {
	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [middleName, setMiddleName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [address, setAddress] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid, setIsMobileValid] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);
	useEffect(() => {
		if (mobileNo.length === 11) 	
		{
			setIsMobileValid(true);
			if (password1 === password2 && password1 !== '' && password2 !== '') {
				setIsMatched(true);
				if (firstName !=='' && middleName !== '' && lastName !=='' && email !=='' && address !== '') {
					setIsAllowed(true);
					setIsActive(true);
				} else {
					setIsAllowed(false);
					setIsActive(false);
				}
				
			} else {
				setIsMatched(false);
				setIsActive(false);
				setIsAllowed(false);
			}
		
		} 
		else if(password1 !== '' && password1 === password2) {
			setIsMatched(true);
		}
		else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		};

	}, [firstName, lastName, middleName, email, address, password1, password2, mobileNo]);
	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()

		const isRegistered = await fetch(`https://thawing-sea-49056.herokuapp.com/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						middleName: middleName,
						lastName: lastName,
						address: address,
						email: email,
						password: password1,
						mobileNumber: mobileNo
					})
				}).then(res => res.json()).then(data => {
					console.log(data)
					if (data.email) {
						return true;
					} else {
						//failure
						return false;
					}
				})
			if (isRegistered) {
				setFirstName('');
				setMiddleName('');
				setLastName('');
				setEmail('');
				setAddress('');
				setMobileNo('');
				setPassword1('');
				setPassword2('');				 
				await Swal.fire(
						{
							icon:"success",
							title:"Registration Successful!",
							text: "Thank you for creating an account!"

						}
					)
				//display a message that will confirm to the user that registration is successful
				window.location.href = '/login'
			} else {
				//response if registration has failed
				await Swal.fire(
						{
							icon:"error",
							title:"Registration Error!",
							text: "Try Again Later!"

						}
					)
			}				 
			
	};

	return(
		user.id
		?
			<Navigate to="/products" replace={true} />
		:

		<>
			<Hero bannerData={data}/>
			<Row>
				<Col md={5} className="ml-auto mr-auto">
					<Container className="bgRegister ml-auto mr-auto text-left pb-2">
						{
							isAllowed ? 
							<h1 className="text-center text-success">You May Now Register!</h1>
							:
							<h1 className="text-center">Register Form</h1>
						}
						<h6 className="mt-3 text-center text-secondary">Fill up the form below</h6>
						<Form onSubmit={e => registerUser(e)}>
							{/*First Name Field*/}
							<Form.Group>
								<Form.Label>First Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter Your First Name" required value={firstName} onChange={event => {setFirstName(event.target.value)} } />
							</Form.Group>

							{/*Middle Name Field*/}
							<Form.Group>
								<Form.Label>Middle Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter Your Middle Name" required value={middleName} onChange={event => {setMiddleName(event.target.value)} } />
							</Form.Group>

							{/*Last Name Field*/}
							<Form.Group>
								<Form.Label>Last Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter Your Last Name" required value={lastName} onChange={event => {setLastName(event.target.value)} } />
							</Form.Group>

							{/*Email*/}
							<Form.Group>
								<Form.Label>Email: </Form.Label>
								<Form.Control type="email" placeholder="Enter Your Email" required value={email} onChange={event => {setEmail(event.target.value)} } />
							</Form.Group>

						{/*Adress*/}
							<Form.Group>
								<Form.Label>Address: </Form.Label>
								<Form.Control type="text" placeholder="Enter Your Address" required value={address} onChange={event => {setAddress(event.target.value)} } />
							</Form.Group>

							{/*Mobile*/}
							<Form.Group>
								<Form.Label>Mobile Number: </Form.Label>
								<Form.Control type="number" placeholder="Enter Your Mobile Number" required value={mobileNo} onChange={event => {setMobileNo(event.target.value)} }/>
								{
									isMobileValid ?
								<span className="text-success">Mobile No. is now Valid!</span>
								:
								<span className="text-light">Mobile No. Should be 11-digits</span>
								}
							</Form.Group>

							{/*Password*/}
							<Form.Group>
								<Form.Label>Password: </Form.Label>
								<Form.Control type="password" placeholder="Enter Your Password" required value={password1} onChange={event => {setPassword1(event.target.value)} }/>
							</Form.Group>

							{/*Reconfirm Password*/}
							<Form.Group>
								<Form.Label>Reconfirm Password: </Form.Label>
								<Form.Control type="password" placeholder="Confirm Your Password" required value={password2} onChange={event => {setPassword2(event.target.value)} }/>
									{
										isMatched ? 
									<span className="text-success">Passwords Match!</span>
										:
									<span className="text-danger">Passwords Should Match!</span>	
									}
							</Form.Group>

							{/*Register Button*/}
							{
								isActive ? 

								<Button variant="success" className="btn-block" type="submit">Register
								</Button>
									:
								<Button variant="success" className="btn-block" disabled>Register
								</Button>
							}

						</Form>
					</Container>
					
				</Col>
			</Row>
			<Footer />
		</>
		);

}