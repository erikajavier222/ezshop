import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate, Link } from 'react-router-dom';
import Hero from './../components/Banner';
import {Form, Button, Container, Card, Col, Row} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Footer from './../components/Footer';

const data = {
	title: 'Welcome to Login',
	
}

export default function Login() {
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');	
	
	let addressSign = email.search('@');
	let dns = email.search('.com')

	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);

	useEffect(() => {
			if (dns !== -1 & addressSign !== -1) {
				setIsValid(true);
				if (password !== '') {
					setIsActive(true)
				} else {
					setIsActive(false);
				}
			} else {
				setIsValid(false);
			}
		},[email, password, dns, addressSign]);

	const loginUser = async (event) => {
		event.preventDefault()

		fetch('https://thawing-sea-49056.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
						email: email,
						password: password
						})
		}).then(res => res.json()).then(data => {
			let token = data.access;

			if (typeof token !== 'undefined') {

				localStorage.setItem('access' ,token);
				fetch('https://thawing-sea-49056.herokuapp.com/users/details', {
				  headers: {
				    Authorization: `Bearer ${token}`
				  }
				}).then(res => res.json()).then(convertedData => {
				  if (typeof convertedData._id !== 'undefined') {
				    setUser({
				      id: convertedData._id,
				      isAdmin: convertedData.isAdmin
				    })
						Swal.fire({
							icon:"success",
							title:"Login Successful!",
							text: "Welcome!"
						});
				  } else {
				    setUser({
				      id: null,
				      isAdmin: null
				    });
				  }
				});
			} else {
				Swal.fire({
					icon:"error",
					title:"Login Error!",
					text: "Check Your Credentials!"
				})
			}
		});
		
	};

	return(
		user.id
		?
			<Navigate to = "/products" replace ={true} />
		:
			
		<>
			<Card>
				<Card.Img src="" />
			</Card>
			<Hero bannerData={data}/>
			<Row>
				
			<Col md={5} className="ml-auto mr-auto p-5">
				
			<Container className="bgLogin text-left">
				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Email Address*/}
					<Form.Group>
						<Form.Label className="">Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email Here" required value={email} onChange={event => {setEmail(event.target.value)} }/>
						{
							isValid ? 
								<h6 className="text-success">Email is Valid!</h6>
							:
								<h6 className="text-mute">Email is Invalid!</h6>
						}
					</Form.Group>

					{/*Password*/}
					<Form.Group>
						<Form.Label className="">Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Password Here" required value={password} onChange={event => setPassword(event.target.value)} />
					</Form.Group>
					{
						isActive ?
							<Button type="submit" variant="success" className="btn-block">Login</Button>
						:
							<Button type="submit" variant="secondary" className="btn-block" disabled>Login</Button>
					}
					<Form.Label>Don't have an account yet?<Link to="/register"> Click Here!</Link></Form.Label>
				</Form>
			</Container>
			</Col>
			{/*<Col>
				<Img src="" />
			</Col>*/}
			</Row>
			<Footer />
		</>
		);
}